
package scenes;
 
import com.haxepunk.HXP;
import com.haxepunk.Scene;
import com.haxepunk.utils.Key;
import com.haxepunk.utils.Input;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Text;
 
class TitleScreen extends Scene
{

    public function new()
    {
        super();
        var options:TextOptions = {size:60, color:0xFFFFFF};
        var debugText = new Text("FAQ DA BATCH BAWL", 165, 200, 0, 0, options);
        debugText.centerOO();
        debugText.smooth = false;
        var e:Entity = new Entity(150,250);
        e.graphic = debugText;
        add(e);
    }

    public override function update()
    {
        super.update();
        if(Input.pressed(Key.ANY))
        {
            HXP.scene = new scenes.GameScene();
        }
    }
}