
package scenes;
 
import com.haxepunk.Scene;
import com.haxepunk.Entity;
import com.haxepunk.graphics.Text;
import com.haxepunk.HXP;
import entities.BeachBall;
import entities.Player;
import entities.Wall;
import entities.ComboMeter;

class GameScene extends Scene
{
	public function new()
	{
		super();
	}

	public override function begin()
	{
		add(new BeachBall(50,50));
		add(new Player(50,60));
		add(new Wall(-HXP.width,0));
		add(new Wall(HXP.width,0));
		add(new Wall(0,-HXP.height));
		add(new Wall(0,HXP.height));
		add(new ComboMeter());
	}

	public override function update()
	{
		super.update();
	}

}