
package entities;

import com.haxepunk.Entity;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.masks.Circle;
import com.haxepunk.utils.Input;
import com.haxepunk.utils.Key;
import com.haxepunk.graphics.Spritemap;
import entities.Fist;
class Player extends Entity
{
	public function new(x:Float, y:Float)
    {
 		image = new Spritemap("gfx/jackson.png", 43, 44, shit);
        image.add('idle',[0,5,0],3);
        image.add('move',[5,4],12);
        image.add('punch',[1,1,2,2,2,2,2,2,1,1,1],30,false);
        image.play('idle');
        image.scale = 10;
        image.x = -40;
        image.y = -20;
        super(x, y);
        graphic = image;
        type = "player";
        currentAnimation = 'idle';
    }

    private function shit()
    {
        currentAnimation = 'idle';
    }

    public override function update()
    {
        super.update();
        if(currentAnimation != 'punch')
        {
     		if(Input.check(Key.LEFT) && x > -100)
     		{
     			x -= 2;
                currentAnimation = 'move';
     		}
     		else if(Input.check(Key.RIGHT) && x < 250)
     		{
     			x += 2;
                currentAnimation = 'move';
     		}
            else
            {
                currentAnimation = 'idle';
            }
            if(Input.pressed(Key.SPACE))
            {
                scene.add(new Fist(x + 205,y + 140));
                currentAnimation = 'punch';
                image.play(currentAnimation,true);
            }
        }
        image.play(currentAnimation);
    }

    private var image:Spritemap;
	private var currentAnimation:String;
}