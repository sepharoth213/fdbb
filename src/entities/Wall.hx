
package entities;

import com.haxepunk.Entity;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.masks.Circle;

class Wall extends Entity
{
	public function new(x:Float, y:Float)
    {
        super(x, y);
        setHitbox(HXP.width,HXP.height);
        type = "wall";
    }

}