
package entities;

import com.haxepunk.Entity;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.masks.Circle;
import com.haxepunk.graphics.Spritemap;

class BeachBall extends Entity
{
	public function new(x:Float, y:Float)
    {
 		
        image = new Spritemap("gfx/beachball.png", 84, 90);
        image.add('idle',[0,1,2,3,4,5],12);
        image.play('idle');
		// image.centerOO();
		// image.x += image.originX;
		// image.y += image.originY;
        image.scale = 2;
        super(x, y);
        mask = new Circle(58,24,5);
        graphic = image;
        xSpeed = 3;
        ySpeed = 0;
        type = "ball";
    }

    public function hit(strength:Float)
    {
        if(xSpeed < 0)
        {
            xSpeed = -xSpeed;
        }
        xSpeed += strength;
    }

    public override function update()
    {
        moveBy(xSpeed,0,'wall');
 		moveBy(0,ySpeed,'wall');
 		ySpeed += gravity;
        xSpeed *= friction;
        // ySpeed *= friction;
    }

    public override function moveCollideX(e:Entity)
    {
        xSpeed = -xSpeed;
        return true;
    }

    public override function moveCollideY(e:Entity)
    {
        ySpeed = -ySpeed;
        return true;
    }

    private var image:Spritemap;
    public var xSpeed:Float;
    public var ySpeed:Float;
    private var gravity = .2;
    private var friction = .999;
	

}