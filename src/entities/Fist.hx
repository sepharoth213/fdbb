
package entities;

import com.haxepunk.Entity;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.masks.Circle;

class Fist extends Entity
{
    public function new(x:Float, y:Float)
    {
        super(x, y);
        setHitbox(50,40);
        image =  Image.createRect(width,height,0xdead,0);
        image.centerOO();
        image.x += image.originX;
        image.y += image.originY;
        graphic = image;
        xSpeed = 40;

        duration = 3;
        type = "fist";
        hits = 0;
    }

    public override function update()
    {
        x += xSpeed;
        duration -= 1;
        super.update();
        if(duration < 0)
        {
            HXP.scene.remove(this);
        }
        else
        {
            var e = collide("ball",x,y);
            if(e != null)
            {
                var ball = cast(e,entities.BeachBall);
                ball.hit(3);
                hits += 1;
            }
        }
    }

    private var image:Image;
    private var duration:Int;
    private var gravity = .1;
    private var xSpeed:Float;
    private var hits:Int;
    
}