
package entities;

import com.haxepunk.Entity;
import com.haxepunk.HXP;
import com.haxepunk.graphics.Image;
import com.haxepunk.graphics.Graphiclist;
import entities.BeachBall;

class ComboMeter extends Entity
{
    public function new()
    {
        super(0, 0);
        target = 50;
        progress = 0;
        // var bg = Image.createRect(HXP.width,40,0x333333);

        image = Image.createRect(HXP.width,38,0xdead);
        image.y += 1;
        // var list = new Graphiclist();
        // list.add(bg);
        // list.add(image);
        graphic = image;

    }

    public override function update()
    {
        super.update();
        var balls = new Array<Entity>();
        scene.getType( 'ball' , balls );
        var ball = cast(balls[0],BeachBall);
        progress = Math.abs(ball.xSpeed);
        image.scaleX = progress/target;
        if(image.scaleX > 1)
        {
            
        }
    }

    private var progress:Float;
    private var target:Float;
    private var image:Image;
}