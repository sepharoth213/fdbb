
import com.haxepunk.Engine;
import com.haxepunk.HXP;
import scenes.GameScene;

class Main extends Engine
{

	override public function init()
	{
		// HXP.console.enable();
		HXP.scene = new scenes.TitleScreen();
	}

	public static function main() { new Main(); }

}